# Telegram desktop icons
	
 Monochrome icons for Telegram desktop in Plasma 5 using the Breeze desktop theme. 
 
## Observation
There is not message counter.
 
## Installation
  Copy the directory `ticons` into `tdata/` in the Telegram installation directory. 
 For dark themes, copy `ticons-white` in to `tdata/` then change directory name from `ticons-white` to `ticons` and override the files.
In some distributions you can find it in `/opt/tdesktop` or  `/opt/TelegramDesktop` or wherever that you put the installation directory. Is possible to find the TelegramDesktop/ directory in `/home/.local/usr/share/`
## Views
 
 - **General icon** 

 ![General icon]
 (https://i.imgur.com/Q2gY7ch.png) <br />
 
 - **Incoming messages**  

![Incoming messages]
 (https://i.imgur.com/h1eRErG.png) <br />
 
 - **Muted chats**
 
 ![Muted chats]
 (https://i.imgur.com/7nkaI7F.png) <br />
